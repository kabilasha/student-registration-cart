<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Student;
use App\Models\Course;

class StudentController extends Controller
{
    //
    function index()
    {
        $students=Student::all();
        return view('index',['students'=>$students]);
    }
    function create()
    {
      $courses=Course::all();
      return view('student',['courses'=>$courses]);
   }
     public function store(Request $req)
     {

         $validated = $req->validate([
             'firstname' => 'required|max:25',
             'lastname' => 'required|max:25',
             'email' => 'required',

         ]);




         $student = new Student;
         $student->firstname =$req->firstname;
         $student->lastname = $req->lastname;
         $student->email = $req->email;
         $student->course_id = $req->course_id;

         $student->save();
         return redirect('Students');



     }

}
