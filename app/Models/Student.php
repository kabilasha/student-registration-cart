<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Student extends Model
{
    use HasFactory;
    public $timestamps=false;
    protected $student = 'students';

    public function course()
    {
        return $this->belongsTo( Course::class);
    }
}
