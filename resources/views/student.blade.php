<!DOCTYPE html>
<html lang="en">

<head>
    <title></title>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <script src="https://cdn.tailwindcss.com"></script>
</head>
<body>
<!--
  This example requires some changes to your config:

  ```
  // tailwind.config.js
  module.exports = {
    // ...
    plugins: [
      // ...
      require('@tailwindcss/forms'),
    ],
  }
  ```
-->
<div class="isolate bg-white px-6 py-24 sm:py-32 lg:px-8">
    <div class="absolute inset-x-0 top-[-10rem] -z-10 transform-gpu overflow-hidden blur-3xl sm:top-[-20rem]" aria-hidden="true">
        <div class="relative left-1/2 -z-10 aspect-[1155/678] w-[36.125rem] max-w-none -translate-x-1/2 rotate-[30deg] bg-gradient-to-tr from-[#ff80b5] to-[#9089fc] opacity-30 sm:left-[calc(50%-40rem)] sm:w-[72.1875rem]" style="clip-path: polygon(74.1% 44.1%, 100% 61.6%, 97.5% 26.9%, 85.5% 0.1%, 80.7% 2%, 72.5% 32.5%, 60.2% 62.4%, 52.4% 68.1%, 47.5% 58.3%, 45.2% 34.5%, 27.5% 76.7%, 0.1% 64.9%, 17.9% 100%, 27.6% 76.8%, 76.1% 97.7%, 74.1% 44.1%)"></div>
    </div>
    <div class="mx-auto max-w-2xl text-center">
        <h2 class="text-3xl font-bold tracking-tight text-gray-900 sm:text-4xl">STUDENT REGISTRATION  FORM</h2>
        <p class="mt-2 text-lg leading-8 text-gray-600"></p>
    </div>
    <form action="" method="POST" class="mx-auto mt-16 max-w-xl sm:mt-20">
       @csrf
        <div class="grid grid-cols-1 gap-x-8 gap-y-6 sm:grid-cols-2">
            <div class="sm:col-span-2">

                <div>
                    <label for="firstname" class="block text-sm font-semi-bold leading-6 text-gray-900">First name</label>
                    <div class="mt-2.5">
                        <input type="text" name="firstname" id="firstname" value="{{old('firstname')}}" autocomplete="given-name" class="block w-full rounded-md border-0 px-3.5 py-2 text-gray-900 shadow-sm ring-1 ring-inset ring-gray-300 placeholder:text-gray-400 focus:ring-2 focus:ring-inset focus:ring-indigo-600 sm:text-sm sm:leading-6">
                    <span style="color: firebrick">@error('firstname'){{$message}}@enderror</span>
                    </div>
                </div>
                <div>
                    <label for="lastname" class="block text-sm font-semi-bold leading-6 text-gray-900">Last name</label>
                    <div class="mt-2.5">
                        <input type="text" name="lastname" id="lastname" value="{{old('lastname')}}" autocomplete="family-name" class="block w-full rounded-md border-0 px-3.5 py-2 text-gray-900 shadow-sm ring-1 ring-inset ring-gray-300 placeholder:text-gray-400 focus:ring-2 focus:ring-inset focus:ring-indigo-600 sm:text-sm sm:leading-6">
                        <span style="color: firebrick">@error('lastname'){{$message}}@enderror</span>
                    </div>
                </div>

            </div>
            <div class="sm:col-span-2">
                <label for="email" class="block text-sm font-semi-bold leading-6 text-gray-900">Email</label>
                <div class="mt-2.5">
                    <input type="email" name="email" id="email" value="{{old('email')}}" autocomplete="email" class="block w-full rounded-md border-0 px-3.5 py-2 text-gray-900 shadow-sm ring-1 ring-inset ring-gray-300 placeholder:text-gray-400 focus:ring-2 focus:ring-inset focus:ring-indigo-600 sm:text-sm sm:leading-6">
                    <span style="color: firebrick">@error('email'){{$message}}@enderror</span>
                </div>
            </div>

            <div class="sm:col-span-2">
                <label for="course" class="block text-sm font-semi-bold leading-6 text-gray-900">Course </label>
                <div class="relative mt-2.5">
                    <div class="absolute inset-y-0 left-0 flex items-center">
                        <label for="course" class="sr-only">select1</label>
                        <select id="course_id" name="course_id" class="h-full rounded-md border-0 bg-transparent bg-none py-0 pl-4 pr-9 text-gray-400 focus:ring-2 focus:ring-inset focus:ring-indigo-600 sm:text-sm">
                            @foreach($courses as $course)
                            <option value ="{{$course->id}}">{{$course->name}}</option>
                            @endforeach

                        </select>

                    </div>
                    <input type="tel" name="course" id="course" autocomplete="tel"  value="{{old('email')}}" class="block w-full rounded-md border-0 px-3.5 py-2 pl-20 text-gray-900 shadow-sm ring-1 ring-inset ring-gray-300 placeholder:text-gray-400 focus:ring-2 focus:ring-inset focus:ring-indigo-600 sm:text-sm sm:leading-6">

                </div>





            <div class="mt-10">
                <button type="submit" class="block w-full rounded-md bg-purple-950 px-3.5 py-2.5 text-center text-sm font-semi-bold text-white shadow-sm hover:bg-purple-950 focus-visible:outline focus-visible:outline-2 focus-visible:outline-offset-2 focus-visible:outline-indigo-600">Submit</button>
            </div>
        </div>
        </div>
    </form>
</div>
</body>
</html>
