
<!DOCTYPE html>
<html lang="en">

<head>
    <title></title>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <script src="https://cdn.tailwindcss.com"></script>
</head>
<body>

<div class="p-2 border-r cursor-pointer text-sm font-thin text-slate-950">
    <div class="flex items-left justify-left">
    <a href="Student-Register" class="focus:outline-none text-wh   ite bg-teal-600 hover:bg-green-800 focus:ring-4 focus:ring-green-300 font-medium rounded-lg text-sm px-5 py-2.5 mr-2 mb-2 dark:bg-green-600 dark:hover:bg-green-700 dark:focus:ring-green-800">ADD</a>
    </div>

            <div class="text-4xl font-extra-bold dark:text-white">
                <hi> REGISTERED STUDENTS LIST</hi>
                    </div>
<div class="table w-full p-2">
    <table class="w-full border">
        <thead>
        <tr class="bg-teal-600 border-b text-sm font-thin text-slate-950">

            <th class="p-2 border-r cursor-pointer text-sm font-thin text-slate-950">
                <div class="flex items-center justify-center">
                    <div class="text-4xl font-extra-bold dark:text-white">
                        ID
                    </div>
                </div>
            </th>
            <th class="p-2 border-r cursor-pointer text-sm font-thin text-slate-950">
                <div class="flex items-center justify-center">
                    <div class="text-4xl font-extra-bold dark:text-white">
                    First Name
                    </div>
                </div>
            </th>

            <th class="p-2 border-r cursor-pointer text-sm font-thin text-slate-950">
                <div class="flex items-center justify-center">
                    <div class="text-4xl font-extra-bold dark:text-white">
                    Last Name
                    </div>
                </div>
            </th>
            <th class="p-2 border-r cursor-pointer text-sm font-thin text-slate-950">
                <div class="flex items-center justify-center">
                    <div class="text-4xl font-extra-bold dark:text-white">
                    Email
                    </div>
                </div>
            </th>
            <th class="p-2 border-r cursor-pointer text-sm font-thin text-slate-950">
                <div class="flex items-center justify-center">
                    <div class="text-4xl font-extra-bold dark:text-white">
                    Course
                    </div>
                </div>
            </th>
            <th class="p-2 border-r cursor-pointer text-sm font-thin text-slate-950">
                <div class="flex items-center justify-center">
                    <div class="text-4xl font-extra-bold dark:text-white">
                    Action
                    </div>
                </div>
            </th>
        </tr>
        </thead>
        <tbody>
        <tr class="bg-gray-50 text-center">
            <td class="p-2 border-r">



        </tr>
        @foreach($students as $student)
        <tr class="bg-gray-400 text-center border-b text-sm text-gray-950">


            <td class="p-2 border-r">{{ $student['id']}}}</td>
            <td class="p-2 border-r">{{ $student['firstname']}}}</td>
            <td class="p-2 border-r">{{ $student['lastname']}}}</td>
            <td class="p-2 border-r">{{ $student['email']}}}</td>
            <td class="p-2 border-r">{{ $student->course->name}}</td>
            <td>
                <a href="#" class="bg-teal-600 p-2 text-gray-950 hover:shadow-lg text-xs font-thin">Edit</a>
                <a href="#" class="bg-teal-600 p-2 text-gray-950 hover:shadow-lg text-xs font-thin">Delete</a>
            </td>
        </tr>
         @endforeach
        </tbody>
    </table>
</div>
</div>
</body>
</html>
